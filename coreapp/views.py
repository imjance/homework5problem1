import datetime
from coreapp import app
from flask import Flask, request

weekdays = {0: 'Monday', 1: 'Tuesday', 2: 'Wednesday', 3: 'Thursday', 4: 'Friday',
            5: 'Saturday', 6: 'Sunday'}

app = Flask(__name__)

@app.route('/')
def index():
    page = """<!DOCTYPE html>
    <html lang="en-US">
    <head>
        <meta charset="utf-8">
        <title>Hello World Page</title>
    </head>
    <body>
        <h1>Enter a date</h1>
        <p>Enter a date below and submit and we will find the day of the week.</p></br>

        <form action="dow" method="get">
            <select name="day" id="day">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
            </select>

            <select name="month" id="month">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
            </select>

            <select name="year" id="year">
                <option value="2014">2014</option>
                <option value="2015">2015</option>
                <option value="2016">2016</option>
                <option value="2017">2017</option>
                <option value="2018">2018</option>
                <option value="2019">2019</option>
                <option value="2020">2020</option>
            </select>

            <input type="submit" value="Submit Query" />
        </form>

        <form action="">

    </body>
    </html>
    """
    return page

@app.route('/dow')
def dow():
    day_num = int(request.args.getlist('day')[0])
    month_num = int(request.args.getlist('month')[0])
    year_num = int(request.args.getlist('year')[0])

    try:
        weekday_num = datetime.date(year_num, month_num, day_num).weekday()
    except Exception as e:
        return str(e)

    page = """
    <!DOCTYPE html>
    <html lang="en-US">
    <head>
        <title>Day of the week.</title>
        <meta charset="utf-8">
    </head>
    <body>
        <h1>Day of the Week</h1>
        <p>""" + weekdays[weekday_num] + """</p>
    </body>
    </html>
    """

    return page

if __name__ == '__main__':
    app.run(debug = True, port=5001)
